<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;
// Get product options
$options = get_post_meta( get_the_ID(), '_custom_wc_thumb_options', true );
$options2 = get_post_meta( get_the_ID(), '_custom_wc_options', true );
$layout = cs_get_option( 'wc-style' ) ;
$columns_layout = "";
if( isset( $_GET['columns'] ) && $_GET['columns'] == "2" ){ 
    $columns_layout = "6";
}elseif(isset( $_GET['columns'] ) && $_GET['columns'] == "3") {
    $columns_layout = "4";
}elseif(isset( $_GET['columns'] ) && $_GET['columns'] == "4") {
    $columns_layout = "3";
}else {
    $columns_layout =  cs_get_option( 'wc-column' ) ;
};
$content_layout =  cs_get_option( 'content-inner' ) ;
$metro = '';
   $attributes = $product->get_attributes(); 
if ( ! $attributes ) {
       $attributes = "";
}
// Flip thumbnail
$flip_thumb =  cs_get_option( 'wc-flip-thumb' );
if ( isset( $options['wc-thumbnail-size'] ) && $options['wc-thumbnail-size']  && $layout == 'metro'  )  {
	$large = 2;
	$metro = ' metro-item';
} else {
	$large = 1;
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;
$class_see = '';
if($content_layout) {
    $class_see = "inner-flip";
}
$start_row = $end_row = $woo_columns = '';

// Extra post classes
$classes = array();
$classes[] = 'tb-product-items';

?>
<?php
 
		$woo_columns = "tb-products-grid ".$class_see." col-md-" . (int) $columns_layout * $large . $metro . " col-sm-6 col-xs-12 col-xs-66 ";


?>

<div class="row" style="position:relative;">
<div class="col-md-12 col-sm-12 skh_row">
        <div class="col-md-6 col-sm-6 skh_first">
                <?php

                echo '<a  href="' . esc_url( get_permalink() ) . '">';
                    /**
                     * woocommerce_before_shop_loop_item_title hook.
                     *
                     * @hooked woocommerce_show_product_loop_sale_flash - 10
                     * @hooked woocommerce_template_loop_product_thumbnail - 10
                     */
                    // do_action( 'woocommerce_before_shop_loop_item_title' );
                    // if ( $flip_thumb ) {
                    // if ( version_compare( WC_VERSION, '3.0.0', '<' ) ) {
                    //     $attachment_ids = $product->get_gallery_image_ids();
                    // } else {
                    //     $attachment_ids = $product->get_gallery_image_ids();
                    // }
                    // if ( isset( $attachment_ids[0] ) ) {

                    //     $attachment_id = $attachment_ids[0];

                    //     $title = get_the_title();
                    //     $link  = get_the_permalink();
                    //     $image = wp_get_attachment_image( $attachment_id, 'shop_catalog' );

                    //     echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="gallery" title="%s">%s</a>', $link, $title, $image ), $attachment_id, $post->ID );
                    // }
                    // }

                    the_post_thumbnail('jws-imge-related_post');
                echo '</a>';

               

                ?>
        </div>
        <div class="col-sm-6 col-md-6 skh_second">
        <div class="item-top">
            <h6 class="product-title"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
            <?php //do_action( 'woocommerce_after_shop_loop_item' ); ?>
        </div>
        <div class="item-bottom">
		<?php
        do_action('woocommerce_template_loop_price');
        $rating_count = $product->get_rating_count();
        if ( $rating_count > 0 ) {
               echo wc_get_rating_html( $product->get_average_rating() ); 
                ?> <span>( <?php echo $rating_count; esc_html_e(" reviews" , "kitgreen");  ?> )</span><?php 
        }
        
		?>
        </div>
        <div class="inner">
        <div class="btn-inner-center">
        
        
            <?php 
            // To display wishlist and cart
                // do_action('woocommerce_template_loop_add_to_cart');
                // echo '<a href="' . $product->get_permalink() . '"  class="product-quick-view"><span class="ion-android-search"></span></a>';
                // if( function_exists('YITH_WCWL') ){
                //     echo kitgreen_wishlist_btn();
                // }  
             ?>
             
        </div>
        <?php if($content_layout): ?>
        <div class="content-inner-bt">
             <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
             <p><?php echo the_excerpt(); ?> </p>
             
             <p><?php echo the_content(); ?> </p>
            <?php
            do_action('woocommerce_template_loop_price');  
    		?>
        </div>
        <?php endif; ?>
        </div> 
        <div class="description">

<div class="desc-left">
        <?php 
            the_excerpt();
        ?>
    <div class="info-product">
    <?php do_action('woocommerce_template_single_meta');?>
    </div>
</div>
<div class="desc-right">
    <a class="button" href="<?php the_permalink(); ?>">Leran More</a>
    <?php   
    // do_action( 'woocommerce_template_single_add_to_cart' );
    ?>
    <?php 
    // jws_kitgreen_wc_add_extra_link_after_cart(); 
    ?>
    
</div>

</div>          
	</div>
</div>
</div>

<!-- EDITING ENDS HERE -->
