<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

global $product, $woocommerce_loop;
// Get product options
$options = get_post_meta(get_the_ID(), '_custom_wc_thumb_options', true);
$options2 = get_post_meta(get_the_ID(), '_custom_wc_options', true);
$layout = cs_get_option('wc-style');
$columns_layout = "";
if (isset($_GET['columns']) && $_GET['columns'] == "2") {
    $columns_layout = "6";
} elseif (isset($_GET['columns']) && $_GET['columns'] == "3") {
    $columns_layout = "4";
} elseif (isset($_GET['columns']) && $_GET['columns'] == "4") {
    $columns_layout = "3";
} else {
    $columns_layout = cs_get_option('wc-column');
}
;
$content_layout = cs_get_option('content-inner');
$metro = '';
$attributes = $product->get_attributes();
if (!$attributes) {
    $attributes = "";
}
// Flip thumbnail
$flip_thumb = cs_get_option('wc-flip-thumb');
if (isset($options['wc-thumbnail-size']) && $options['wc-thumbnail-size'] && $layout == 'metro') {
    $large = 2;
    $metro = ' metro-item';
} else {
    $large = 1;
}

// Ensure visibility
if (!$product || !$product->is_visible()) {
    return;
}

$class_see = '';
if ($content_layout) {
    $class_see = "inner-flip";
}
$start_row = $end_row = $woo_columns = '';

// Extra post classes
$classes = array();
$classes[] = 'tb-product-items';

?>
<?php

$woo_columns = "tb-products-grid " . $class_see . " col-md-" . (int) $columns_layout * $large . $metro . " col-sm-6 col-xs-12 col-xs-66 ";

?>

<div class="row" style="position:relative;">
<div class="col-md-12 col-sm-12 skh_row">
        <div class="cate-item">

            <!--Title and description-->
            <div class="text-container">
                <div class="text-copy-wrapper">
                    <h2 class="product-title"> <a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                    <?php $content = get_the_excerpt(); ?>
                    <p><?php echo mb_strimwidth($content, 0, 100, '........');?></p>
                </div>
                <div class="button-wrapper">
                    <a class="link" href="<?php the_permalink();?>">See More <i class="fa fa-angle-double-right"></i></a>
                </div>
                
                
            </div>

            <!--Image-->
            <div class="product-image-container">
                <?php
                    echo '<a  href="' . esc_url(get_permalink()) . '">';
                    the_post_thumbnail('large');
                    echo '</a>';
                ?>
            </div>

        </div>

</div>
</div>

<!-- EDITING ENDS HERE -->
