<?php
add_shortcode('productMenu', 'shortcode_productMenu');
function shortcode_productMenu($atts) {
    extract( shortcode_atts( array(
        'product_cat'	=> '',
        'category_link'	=> '',
        'button_text' => ''
	), $atts ) );
    ob_start();
    ?>
<!-- HTML Code Goes Here -->    
<div>

<div class="product-menu-wrapper">
<?php
// Setup your custom query
$args = array( 
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'product_cat' => $product_cat,
    'orderby' => 'rand'

);

$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <div class="product-item">
    
    <a href="<?php echo get_permalink( $loop->post->ID ) ?>">
    <img src="<?php echo the_post_thumbnail_url('shop_catalog');?>"/>
    <span><?php the_title(); ?></span>
    </a>
    </div>




<?php endwhile; wp_reset_query(); // Remember to reset ?>
    <div class="product-item">
    <a class="button" href=" <?php echo $category_link ?>">
   View All <i class="fa fa-arrow-right"></i>
    </a>
    <!-- <span><?php echo $button_text ?></span> -->
   
    </div>
</div>
</div>
<!-- END of HTML Code --> 


<?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [productMenu][/productMenu] **/








add_shortcode('productlist', 'shortcode_productlist');

function shortcode_productlist($atts) {
    extract( shortcode_atts( array(
        'product_cat'	=> '',
	), $atts ) );
    ob_start();
    ?>
<!-- HTML Code Goes Here -->    
<div>

<ul class="sub-menu">
<?php
// Setup your custom query
$args = array( 
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => 7,
    'product_cat' => $product_cat,
    'orderby' => 'rand'

);

$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <li class="list-item">
    
    <a href="<?php echo get_permalink( $loop->post->ID ) ?>">
        <?php the_title(); ?>
    </a>
    </li>

<?php endwhile; wp_reset_query(); // Remember to reset ?>
</ul>
</div>
<!-- END of HTML Code --> 


<?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [productlist product_cat=""][/productlist] **/